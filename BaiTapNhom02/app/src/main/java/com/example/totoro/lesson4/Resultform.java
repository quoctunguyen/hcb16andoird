package com.example.totoro.lesson4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Resultform extends AppCompatActivity {

    TextView tvUsername;
    TextView tvPassword;
    TextView tvRetype;
    TextView tvBirthdate;
    TextView tvGender;
    TextView tvHobbies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultform);

        tvUsername = findViewById(R.id.tvUsername);
        tvPassword = findViewById(R.id.tvPassword);
        tvRetype = findViewById(R.id.tvRetype);
        tvBirthdate = findViewById(R.id.tvBirthdate);
        tvGender = findViewById(R.id.tvGender);
        tvHobbies = findViewById(R.id.tvHobbies);

        Intent intent = getIntent();
        String username = intent.getStringExtra("username");
        String password = intent.getStringExtra("password");
        String retype = intent.getStringExtra("retype");
        String birthdate = intent.getStringExtra("birthdate");
        String gender = intent.getStringExtra("gender");
        String hobbies = intent.getStringExtra("hobbies");

        tvUsername.setText("Username: " + username);
        tvPassword.setText("Password: " + password);
        tvRetype.setText("Retype: " + retype);
        tvBirthdate.setText("Birthdate: " + birthdate);
        tvGender.setText("Gender: " + gender);
        tvHobbies.setText("Hobbies: " + hobbies);

    }
}
