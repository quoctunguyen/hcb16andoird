package com.example.totoro.album.Service;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;

import com.example.totoro.album.Model.Album;
import com.example.totoro.album.Model.Photo;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Totoro on 3/11/2018.
 */

public class DataProvider {

    private Context context;

    public DataProvider(Context context) {
        this.context = context;
    }

    public ArrayList<Photo> getPhotos() {
        ArrayList<Photo> photos = new ArrayList<>();
        Cursor cursor = context.getContentResolver()
                .query(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        null,
                        null,
                        null,
                        MediaStore.Images.Media.DEFAULT_SORT_ORDER);

        cursor.moveToFirst();
        Photo photo;
        while (!cursor.isAfterLast()) {
            photo = new Photo();
            String name = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME));
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            File fileImage = new File(path);
            Date date = new Date(fileImage.lastModified());
            String album = fileImage.getParentFile().getName();
            long size = fileImage.length();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(fileImage.getAbsolutePath(), options);
            int height = options.outHeight;
            int width = options.outWidth;

            photo = new Photo(date, path, name, size, height, width, album);
            photos.add(photo);

            cursor.moveToNext();
        }
        cursor.close();
        return photos;
    }

    public ArrayList<Album> getAlbum() {
        ArrayList<Photo> photos = getPhotos();

        ArrayList<Album> albums = new ArrayList<>();
        Album album = new Album();
        Photo photoTemp = new Photo();
        ArrayList<Photo> photosTemp = new ArrayList<>();

        while (photos.size() > 0) {
            for (int i = 0; i < photos.size(); i++) {
                photoTemp = photos.get(0);
                if (photos.get(i).getAlbum() == photoTemp.getAlbum()) {
                    photosTemp.add(photos.get(i));
                }
            }

            album.setPhotos(photosTemp);
            album.setThumbnail(photoTemp);
            album.setName(new File(photoTemp.getPath()).getParentFile().getName());
            album.setPath(new File(photoTemp.getPath()).getParent());
            albums.add(album);

            for (Photo photo : photos) {
                if (photoTemp.getAlbum() == photo.getAlbum()) {
                    photos.remove(photo);
                }
            }
        }
        return albums;
    }
}
