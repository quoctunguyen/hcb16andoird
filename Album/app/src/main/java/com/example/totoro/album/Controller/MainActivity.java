package com.example.totoro.album.Controller;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.example.totoro.album.R;
import com.example.totoro.album.Service.Constance;
import com.example.totoro.album.Service.DataProvider;
import com.example.totoro.album.Service.StaticMethod;

public class MainActivity extends AppCompatActivity implements PhotoDetailFragment.OnFragmentInteractionListener {

    final int REQUEST_IMAGE_CAPTURE = 1;
    Toolbar toolbar;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initPermission();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitle(R.string.photos);

        StaticMethod.loadFragment(new PhotosFragment(),MainActivity.this);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_photos);
        DataProvider dataProvider = new DataProvider(MainActivity.this);
        Constance.photos = dataProvider.getPhotos();
        //Constance.albums = dataProvider.getAlbum();

        //        // attaching bottom sheet behaviour - hide / show on scroll
//        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
//        layoutParams.setBehavior(new BottomNavigationBehavior());

    }


    public void initPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(MainActivity.this, "Permission isn't granted ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Permisson don't granted and dont show dialog again ", Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_camera:
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivity(intent);
                    return true;
                case R.id.navigation_photos:
                    toolbar.setTitle(R.string.photos);
                    fragment = new PhotosFragment();
                    StaticMethod.loadFragment(fragment, MainActivity.this);
                    return true;
                case R.id.navigation_albums:
                    toolbar.setTitle(R.string.albums);
                    fragment = new AlbumsFragment();
                    StaticMethod.loadFragment(fragment, MainActivity.this);
                    return true;
            }
            return false;
        }
    };


    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(int index) {
        PhotoDetailFragment photoDetailFragment = (PhotoDetailFragment) getSupportFragmentManager().findFragmentById(R.id.photo_detail_fragment);
        if (photoDetailFragment != null || photoDetailFragment.isInLayout()) {
            photoDetailFragment.getPosition(index);
        }else{
            Toast.makeText(this, "fragment is not working!", Toast.LENGTH_SHORT).show();
        }
    }
}
