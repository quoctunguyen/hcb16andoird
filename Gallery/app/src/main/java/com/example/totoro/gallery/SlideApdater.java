package com.example.totoro.gallery;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by Totoro on 2/22/2018.
 */

public class SlideApdater extends PagerAdapter {
    private ArrayList<Photo> photos;
    private int position;
    LayoutInflater inflater;
    Context context;

    public SlideApdater(Context context, ArrayList<Photo> photos, int position) {
        this.photos = photos;
        this.position = position;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = inflater.inflate(R.layout.slide_photo, container, false);
        TextView photo_date = view.findViewById(R.id.photo_date);
        ImageView full_screen_photo = view.findViewById(R.id.full_screen_photo);
        Photo photo = photos.get(position);

        photo_date.setText(photo.getDate().toString());
        Glide.with(this.context).
                load(photo.getPath())
                .into(full_screen_photo);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
