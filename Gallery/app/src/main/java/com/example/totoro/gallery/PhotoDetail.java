package com.example.totoro.gallery;

import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Slide;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class PhotoDetail extends AppCompatActivity {
    private ArrayList<Photo> photos;
    private int position = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);
        photos = new ArrayList<>();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        photos = (ArrayList<Photo>) bundle.getSerializable("photos");
        position = bundle.getInt("position");
        ViewPager slide_photo = findViewById(R.id.slide_photo);
        SlideApdater slideApdater = new SlideApdater(this,photos,position);
        slide_photo.setAdapter(slideApdater);
        slide_photo.setCurrentItem(position);
    }
}
