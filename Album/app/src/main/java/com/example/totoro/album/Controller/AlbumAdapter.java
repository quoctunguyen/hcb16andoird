package com.example.totoro.album.Controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.totoro.album.Model.Album;
import com.example.totoro.album.R;

import java.util.ArrayList;

/**
 * Created by Totoro on 3/11/2018.
 */

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder> {
    private Context context;
    private ArrayList<Album> albums;

    public AlbumAdapter(Context context, ArrayList<Album> albums) {
        this.context = context;
        this.albums = albums;
    }

    @Override
    public AlbumAdapter.AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_album, parent, false);
        return new AlbumAdapter.AlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AlbumAdapter.AlbumViewHolder holder, int position) {
        holder.album_name.setText(albums.get(position).getName());
        Glide.with(context)
                .load(albums.get(position).getPhotos().get(0).getPath())
                .centerCrop()
                .override(240, 240)
                .into(holder.album_thumnail);
        holder.album_size.setText(albums.get(position).getPhotos().size());
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }

    public class AlbumViewHolder extends RecyclerView.ViewHolder {
        TextView album_name;
        ImageView album_thumnail;
        TextView album_size;

        public AlbumViewHolder(View itemView) {
            super(itemView);
            album_name = itemView.findViewById(R.id.album_name);
            album_thumnail = itemView.findViewById(R.id.album_thumnail);
            album_size = itemView.findViewById(R.id.album_size);
        }
    }
}
