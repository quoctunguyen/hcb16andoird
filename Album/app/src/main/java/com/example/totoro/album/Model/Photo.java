package com.example.totoro.album.Model;

import java.io.Serializable;
import java.util.Date;

public class Photo implements Serializable {
    private Date date;
    private String path;
    private String name;
    private long size;
    private float width;
    private float height;
    private String album;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public Photo() {
    }

    public Photo(Date date, String path, String name, long size, float width, float height, String album) {
        this.date = date;
        this.path = path;
        this.name = name;
        this.size = size;
        this.width = width;
        this.height = height;
        this.album = album;
    }
}
