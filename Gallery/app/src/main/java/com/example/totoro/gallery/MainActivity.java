package com.example.totoro.gallery;

import android.Manifest;
import android.app.FragmentManager;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            initPermission();

        File root = Environment.getExternalStorageDirectory();
        ArrayList<File> files = imageReader(root);
        final RecyclerView recycler_view = (RecyclerView) findViewById(R.id.recycler_view);

        GridLayoutManager layoutManager = new GridLayoutManager(MainActivity.this, 3);
        recycler_view.setAdapter(new PhotoAdapter(MainActivity.this, getData(files)));
        recycler_view.setLayoutManager(layoutManager);

        for(int i =0;i<files.size();i++){
            Log.d("tag", "onCreate: "+files.get(i).getPath());
        }

    }

    private ArrayList<Photo> getData(ArrayList<File> files) {
        ArrayList<Photo> photos = new ArrayList<>();
        Photo photo;
        for (File file : files) {
            if (file.getName().endsWith(".jpg") || file.getName().endsWith(".png")) {
                Date date = new Date(file.lastModified());
                String path = file.getPath();
                String name = file.getName();
                long size = file.length();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                int height = options.outHeight;
                int width = options.outWidth;
                String album = file.getParentFile().getName();

                photo = new Photo(date, path, name, size, height, width, album);

                photos.add(photo);
            }
        }
        return photos;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public ArrayList<File> imageReader(File root) {
        ArrayList<File> a = new ArrayList<>();
        Photo photo;
        File[] files = root.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                a.addAll(imageReader(file));
            } else {
                if (file.getName().endsWith(".jpg")) {
                    a.add(file);
                }
            }
        }
        return a;
    }

    public void initPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(MainActivity.this, "Permission isn't granted ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Permisson don't granted and dont show dialog again ", Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

            }
        }
    }
}
