package com.example.totoro.lesson4;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Registerform extends AppCompatActivity {

    EditText edtUsername;
    EditText edtPassword;
    EditText edtRetype;
    EditText edtBirthdate;

    RadioButton rdbMale;
    RadioButton rdbFemale;

    CheckBox ckbTenis;
    CheckBox ckbFutbal;
    CheckBox ckbOthers;

    Button btnSelect;
    Button btnSignUp;
    Button btnReset;

    Intent intent;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registerform);
        initView();
        register();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void register() {
        // birthdate mặc định
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat defaltTime = null;
        defaltTime = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String strDate = defaltTime.format(cal.getTime());
        edtBirthdate.setText(strDate);

        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog pic = new DatePickerDialog(Registerform.this);
                pic.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        edtBirthdate.setText((dayOfMonth) + "/" + (month + 1) + "/" + year);
                    }
                });
                pic.show();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetValue();
            }
        });

        intent = new Intent(this, Resultform.class);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });
    }

    public void resetValue() {
        edtUsername.setText("");
        edtPassword.setText("");
        edtRetype.setText("");
        edtBirthdate.setText("");

        ckbFutbal.setChecked(false);
        ckbTenis.setChecked(false);
        ckbOthers.setChecked(false);

        rdbMale.setChecked(true);
        rdbFemale.setChecked(false);
    }

    public void signUp() {
        String username = edtUsername.getText().toString();
        String password = edtPassword.getText().toString();
        String retype = edtRetype.getText().toString();
        String birthdate = edtBirthdate.getText().toString();

        String gender = "Others";
        if (rdbFemale.isChecked()) {
            gender = rdbFemale.getText().toString();
        } else {
            gender = rdbMale.getText().toString();
        }
        if (rdbMale.isChecked()) {
            gender = rdbMale.getText().toString();
        } else {
            gender = rdbFemale.getText().toString();
        }

        String hobbies = "";

        if (ckbOthers.isChecked() && ckbFutbal.isChecked() && ckbTenis.isChecked()) {
            hobbies += ckbFutbal.getText().toString() + " & ";
            hobbies += ckbTenis.getText().toString() + " & ";
            hobbies += ckbOthers.getText().toString();
        } else if (ckbFutbal.isChecked() && ckbTenis.isChecked()) {
            hobbies += ckbFutbal.getText().toString() + " & ";
            hobbies += ckbTenis.getText().toString();
        } else if (ckbFutbal.isChecked() && ckbOthers.isChecked()) {
            hobbies += ckbFutbal.getText().toString() + " & ";
            hobbies += ckbOthers.getText().toString();
        } else if (ckbTenis.isChecked() && ckbOthers.isChecked()) {
            hobbies += ckbTenis.getText().toString() + " & ";
            hobbies += ckbOthers.getText().toString();
        } else if (ckbTenis.isChecked()) {
            hobbies += ckbTenis.getText().toString();
        } else if (ckbFutbal.isChecked()) {
            hobbies += ckbFutbal.getText().toString();
        } else if (ckbOthers.isChecked()) {
            hobbies += ckbOthers.getText().toString();
        } else {
            hobbies += "Others";
        }

        intent.putExtra("username", username);
        intent.putExtra("password", password);
        intent.putExtra("retype", retype);
        intent.putExtra("birthdate", birthdate);
        intent.putExtra("gender", gender);
        intent.putExtra("hobbies", hobbies);
        startActivity(intent);
    }

    private void initView() {
        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);
        edtRetype = findViewById(R.id.edtRetype);
        edtBirthdate = findViewById(R.id.edtBirthdate);

        rdbMale = findViewById(R.id.rdbMale);
        rdbFemale = findViewById(R.id.rdbFemale);

        ckbFutbal = findViewById(R.id.ckbFutbal);
        ckbTenis = findViewById(R.id.ckbTenis);
        ckbOthers = findViewById(R.id.ckbOthers);

        btnSelect = findViewById(R.id.btnSelect);
        btnSignUp = findViewById(R.id.btnSignUp);
        btnReset = findViewById(R.id.btnReset);
    }
}
