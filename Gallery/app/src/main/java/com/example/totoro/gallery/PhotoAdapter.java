package com.example.totoro.gallery;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {

    Context context;
    ArrayList<Photo> photos;

    public PhotoAdapter(Context context, ArrayList<Photo> photos) {
        this.context = context;
        this.photos = photos;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_photo, parent, false);
        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PhotoViewHolder holder, final int position) {

        Photo photo = photos.get(position);

        Glide.with(context)
                .load(photo.getPath())
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .into(holder.image);

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDetailPhoto(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    private void ShowDetailPhoto(int index) {
        Intent intent = new Intent(context, PhotoDetail.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("photos",photos);
        bundle.putInt("position",index);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        CardView item;
        public PhotoViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            item = itemView.findViewById(R.id.item);
        }
    }

}
